package com.example.springdemo.student;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class FakeDatabase {

    @Getter private final ArrayList<Student> database;

    public FakeDatabase() {
        database = new ArrayList<>();
    }

    public boolean add(Student student) {
        return database.add(student);
    }

}
