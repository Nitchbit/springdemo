package com.example.springdemo.student;

import lombok.*;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@ToString

public class Student {

    @Getter @Setter private Long id;
    @Getter @Setter private String name;
    @Getter @Setter private String email;
    @Getter @Setter private LocalDate date_of_birth;
    @Getter @Setter private Integer age;
}
