package com.example.springdemo.student;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final FakeDatabase db;

    public StudentService() {
        db = new FakeDatabase();
    }

    public List<Student> getStudents() {
        return db.getDatabase();
    }

    public boolean addNewStudent(Student student) {
        return db.add(student);
    }

}
