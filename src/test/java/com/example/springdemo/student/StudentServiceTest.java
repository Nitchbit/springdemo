package com.example.springdemo.student;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StudentServiceTest {

    StudentService studentService = new StudentService();

    Student student1 = new Student(1L, "Nico", "Nico@gmail.com", LocalDate.of(2000, Month.NOVEMBER, 11), 21);

    @Test
    void addNewStudent() {
        studentService.addNewStudent(student1);
        List<Student> temp = studentService.getStudents();
        assertEquals(student1, temp.get(temp.indexOf(student1)));
    }

    @Test
    void getStudents() {

    }

}